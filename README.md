# [Computational geometry] Presentation slides

## Topic: Intersection of convex polygons, star-shaped polygons and half-planes


[The pdf file](./106011225_7.2.1_7.2.2_7.2.4_final_version.pdf) is a copy of the slides.  
For the best experience, please view it at https://docs.google.com/presentation/d/1jSlLNiQUGZj-BavjbnFX5LVey-DUn_ikeezFk6Ouq2I/edit?usp=sharing.
